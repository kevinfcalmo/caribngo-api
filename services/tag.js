export default class Tag {
    constructor({id, label}) {
        this.id = id;
        this.label = label;
    }
}