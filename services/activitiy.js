class Activity {
    constructor(
        { id,
            name,
            description,
            details,
            fb_url,
            insta_url,
            blog_url,
            start_date,
            end_date,
            tags
        }
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.details = details;
        this.fb_url = fb_url;
        this.insta_url = insta_url;
        this.blog_url = blog_url;
        this.start_date = start_date;
        this.end_date = end_date;
        this.tags = tags;
    }
}

export default Activity