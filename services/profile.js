export class Profile {
    constructor({
        id,
        name,
        image_id
    }) {
        this.id = id;
        this.name = name;
        this.image_id = image_id;
    }
}