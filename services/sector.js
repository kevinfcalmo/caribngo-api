export default class Sector {
    constructor({id, label}) {
        this.id = id;
        this.label = label;
    }
}