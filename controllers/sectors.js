import Sector from "../services/sector.js";

export default class Sectors {
    constructor(connexion) {
        this.connexion = connexion
    }
    all() {
        return new Promise((resolve, reject) => {
            var tab = [];
            this.connexion.query('SELECT * FROM sector', (error, result) => {
                if (!error) {
                    result.forEach(element => {
                        tab.push(new Sector({ ...element }))
                    });
                    resolve(tab)
                } else {
                    reject(error)
                }
            })
        })
    }
}