import Tag from "../services/tag.js";

export default class Tags {
    constructor(connexion) {
        this.connexion = connexion
    }
    all() {
        return new Promise((resolve, reject) => {
            var tab = [];
            this.connexion.query('SELECT * FROM tag', (error, result) => {
                if (!error) {
                    result.forEach(element => {
                        tab.push(new Tag({...element}))
                    });
                    resolve(tab)
                } else {
                    reject(error)
                }
            })
        })
    }
}