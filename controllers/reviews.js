import { Review } from "../services/review.js";

export class Reviews {
    constructor(connexion) {
        this.connexion = connexion
    }

    all(id_trip) {
        return new Promise(async (resolve, reject) => {
            var tab = [];
            await this.connexion.query('SELECT * FROM trip_review RIGHT JOIN review ON trip_review.review_id=review.id WHERE trip_review.trip_activity_id = ?', id_trip, (error, result) => {
                if (!error) {
                    result.forEach((review) => {
                        tab.push(new Review({
                            id: review.id,
                            rate: review.rate,
                            message: review.message
                        }))
                    });
                    resolve(tab);
                } else {
                    reject(error);
                }
            })
        })
    }

}