import Trip from '../services/trip.js'

export default class Trips {
    constructor(db) {
        this.db = db
    }

    all = ({profile, tags}) => {
        return new Promise((resolve, reject) => {
            let tab = [];
            let sql = "SELECT * FROM trip";
            let where = "";
            let join = "";
            let values = [];
            let tagArray = tags ? [...tags] : [];

            if (profile) {
                join += " INNER JOIN profile_trip ON trip.id = profile_trip.trip_id"
                if (where === "") {
                    where += " WHERE"
                }
                where += " profile_trip.profile_id = ?"
                values.push(profile)
            }

            if (tags && tagArray.length > 0) {
                join += " INNER JOIN trip_tag ON trip.id = trip_tag.trip_id"
                if (where === "") {
                    where += " WHERE"
                } else {
                    where += " AND "
                }

                where += " trip_tag.tag_id IN (?) "
                values.push([...tags])
            }
            /* Validation des entrées */
            values.forEach((input) => {
                if (!(parseInt(input) > 0)) {
                    reject('Les paramètres ne sont pas au bon format');
                }
            })

            sql += join + where

            this.db.query(sql, values, (error, result) => {
                if (!error) {
                    result.forEach(element => {
                        tab.push(new Trip({...element}))
                    });
                    resolve(tab)
                } else {
                    reject(error)
                }
            })
        })
    }

    one = async (id) => {
        return new Promise((resolve, reject) => {
            this.db.query("SELECT * FROM trip WHERE id = ?", [id], (error, result) => {
                if (!error) {
                    resolve(new Trip({...result[0]}))
                } else {
                    reject(error)
                }
            })
        })
    }
}
