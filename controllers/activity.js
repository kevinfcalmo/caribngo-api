import Activity from "../services/activitiy.js";

export class Activities {
    constructor(connexion) {
        this.connexion = connexion;
    }

    async all({ profile, tags }) {
        return new Promise(async (resolve, reject) => {
            var tagArray = tags ? [...tags] : []
            /* */
            var sql1 = "SELECT activity.id as id, activity.name as name, activity.description as description, activity.details as details, activity.fb_url as fb_url, activity.insta_url as insta_url, activity.blog_url as blog_url, activity.start_date as start_date, activity.end_date as end_date FROM activity";
            var sql2 = "SELECT activity.id as id, activity.name as name, activity.description as description, activity.details as details, activity.fb_url as fb_url, activity.insta_url as insta_url, activity.blog_url as blog_url, activity.start_date as start_date, activity.end_date as end_date, tag.id as tag_id, tag.label as tag_label FROM activity";
            var sql = tagArray.length > 0 ? sql2 : sql1;
            var where = "";
            var join = "";
            var tab = []
            const values = [];


            /* Filtre par profile  */
            if (profile) {
                join += " INNER JOIN trip_activity ON  activity.id = trip_activity.activity_id";
                if (where === '') {
                    where += " WHERE";
                }
                where += " trip_activity.trip_id = ?";
                values.push(profile)
            }

            /* Filtre par tags */
            if (tags && tagArray.length > 0) {
                join += " INNER JOIN activity_tag ON activity.id = activity_tag.activity_id INNER JOIN tag on activity_tag.tag_id =tag.id"
                if (where === "") {
                    where += " WHERE"
                } else {
                    where += " AND "
                }

                where += " activity_tag.tag_id IN (?) "
                values.push([...tags])
            }
            sql += join + where;
            await this.connexion.query(sql, values, (error, result) => {
                if (!error) {
                    for (let i = 0; i < result.length; i++) {
                        /* Si je possède déja au moin une valeur */
                        if (tab.length > 0) {
                            for (let j = 0; j < tab.length; j++) {
                                /* Si je tombe sur une activité que j'ai déja mis dans mon tableau */
                                if (result[i].id === tab[j].id) {
                                    /* Si cette activité possède un tag */
                                    if (result[i].tag_id && result[i].tag_label) {
                                        /* J'ajoute ce tag dans mon objet */
                                        tab[j].tags.push({ id: result[i].tag_id, label: result[i].tag_label })
                                    }
                                } else {
                                    /* Sinon, j'ajoute ma nouvelle activité à mon tableau */
                                    tab.push(new Activity({ ...result[i], tags: [{ id: result[i].tag_id, label: result[i].tag_label }] }))
                                }
                            }
                        }
                        /* Mon tableau de retour est vide */
                        else {
                            /* J'ajoute ma nouvelle activité à mon tableau  */
                            tab.push(new Activity({ ...result[i], tags: [{ id: result[i].tag_id, label: result[i].tag_label }] }))
                        }
                    }
                    /* Je filtres les potentiels doublons */
                    const returnTab = tab.filter((el, index, self) => {
                        /* La condition est validé que si j'ai deux éléments ayant le même id */
                        return index === self.findIndex((t) => (
                            el.id === t.id
                        ))
                    })
                    resolve(returnTab)
                } else {
                    reject(error)
                }
            })
        })
    }

    one(id) {
        return new Promise(async (resolve, reject) => [
            await this.connexion.query('SELECT * FROM activity WHERE id = ?', id, (error, result) => {
                if (!error) {
                    const activity = new Activity({ ...result[0] });
                    resolve(activity);
                } else {
                    reject(error)
                }
            })
        ])
    }
}