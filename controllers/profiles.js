import { Profile } from "../services/profile.js";

export class Profiles {
    constructor(connexion) {
        this.connexion = connexion
    }

    all() {
        return new Promise((resolve, reject) => {
            var tab = [];
            this.connexion.query('SELECT * FROM profile', (error, result) => {
                if (!error) {
                    result.forEach(element => {
                        tab.push(new Profile({ ...element }))
                    });
                    resolve(tab)
                } else {
                    reject(error)
                }
            })
        })
    }

    one(id) {
        return new Promise(async (resolve, reject) => [
            await this.connexion.query('SELECT * FROM profile WHERE id = ?', id, (error, result) => {
                if (!error) {
                    const profile = new Profile({ ...result[0] });
                    resolve(profile);
                } else {
                    reject(error)
                }
            })
        ])
    }
}