
## Exemple de valeur pour tester l'api

```sql
insert into activity(name,description,details,fb_url,insta_url,url_blog,start_date,end_date) values
("Village de la poterie","","","","","",null,null),
("Musée de la banane","","","","","",null,null),
("Habitation Clément","","","","","",null,null);

insert into profile (name) values 
('sportif'),
('culturel'),
('gourmand'),
('festif');

insert into trip(name,activity_id,profile_id)  values
('parcours 1',1,1),
('parcours 2',2,2),
('parcours 3',3,3);

INSERT INTO review(rate,message) VALUES
	 (4,'bien'),
	 (3,'lorem ipsum dolor'),
	 (4,'hello world');

insert into trip_review(trip_activity_id,review_id)  values
(2,1),
(2,2),
(2,3);
```
