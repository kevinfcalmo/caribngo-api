import express from 'express';
import mysql from 'mysql'
import { Activities } from './controllers/activity.js';
import { Profiles } from './controllers/profiles.js';
import { Reviews } from './controllers/reviews.js';
import Trips from './controllers/trips.js';
import Sectors from './controllers/sectors.js';
import Tags from './controllers/tags.js';

const connection = mysql.createConnection({
    host: process.env.RDS_HOSTNAME ?? '127.0.0.1',
    port: process.env.RDS_PORT ?? 3306,
    user: process.env.RDS_USERNAME ?? 'kevin',
    password: process.env.RDS_PASSWORD ?? 'password',
    database: process.env.RDS_DATABASE ?? 'caribngo'
});

connection.connect((err) => {
    if (err) {
        console.error('error connecting:' + err.stack);
    }
    console.log('connected as id:' + connection.threadId);
});

const app = express()
const port = 3001

const activities = new Activities(connection);
const profiles = new Profiles(connection);
const trips = new Trips(connection);
const reviews = new Reviews(connection);
const sectors = new Sectors(connection);
const tags = new Tags(connection);

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "*")
    next()
});

app.get('/activities', async (req, res) => {

    const result = await activities.all({ profile: req.query.profile, tags: req.query.tags })
    res.send({ data: result })

})

app.get('/activities/:id', async (req, res) => {
    const id = req.params.id;
    const result = await activities.one(id);
    res.send({ data: result })
})

app.get('/profiles', async (req, res) => {
    const result = await profiles.all();
    res.send({ data: result })
})

app.get('/profiles/:id', async (req, res) => {
    const result = await profiles.one(req.params.id)
    res.send({ data: result })
})

app.get("/trips", async (req, res) => {
    const result = await trips.all({ profile: req.query.profile, tags: req.query.tags });
    res.send({ data: result })
})

app.get('/trips/:id_profile', async (req, res) => {
    const result = await trips.all(req.params.id_profile);
    res.send({ data: result })
})

app.get('/trips/:id_profile/:id', async (req, res) => {
    const result = await trips.all(req.params.id_profile, req.params.id);
    res.send({ data: result })
})

app.get('/reviews/:id_trip', async (req, res) => {
    const result = await reviews.all(req.params.id_trip)
    res.send({ data: result })
})

app.get('/tags',async(req,res)=>{
    const result = await tags.all();
    res.send({data: result});
})

app.get('/sectors',async(req,res)=>{
    const result = await sectors.all();
    res.send({data: result});
})

app.listen(process.env.PORT ?? port, () => {
    console.log(`Example app listening on port ${port}`)
})
